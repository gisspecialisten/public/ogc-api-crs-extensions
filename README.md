# OGC API CRS Extensions

Testbed project for Geonovum to implement CRS Extension for OGC API Features.
## Scope

Geographic data is everywhere. It is often served with a specific coordinate reference system, referring to the country the data is about. That is useful when that specific coordinate system is also the one you want to use, because for example your data is within the borders of one country. However, what happens when you want to use cross-border data? Then you need data about one country in the coordinate reference system of another country.  

That is why this [Geonovum](https://geonovum.nl) project, by [GIS Specialisten](https://gisspecialisten.nl), demonstrates how spatial data can be used in multiple coordinate reference systems within APIs. Next to that, it shows the required effort to serve data in multiple coordinate reference systems, and request and use data in specific coordinate reference systems.  

This project works with nitrogen deposit and Natura2000 data regarding the Netherlands. This data is used to demonstrate how it can be served in different coordinate systems. 

## License
[<img src="https://www.gnu.org/graphics/gplv3-with-text-136x68.png">](https://www.gnu.org/licenses/gpl-3.0.html)